"""
Usage:
  copace LEVEL TIME [ENTRY]

Cosmic Ocean pace calculator
Level is between 5 and 99
Time is formatted as [HH:]MM:SS[.MS]
"""
from datetime import datetime, timezone
from typing import Optional
import re
import sys
from docopt import docopt

try:
	import readline
except ModuleNotFoundError:
	pass


LEVEL_OFFSET = 5

Time = tuple[int, int, float]


def to_seconds(t: Time) -> float:
	return t[0] * 3600 + t[1] * 60 + t[2]

def parse_time(t: str) -> Optional[Time]:
	if (m := re.match(r'^((\d{1,2}):)?(\d{1,2}):(\d{1,2}(\.(\d+))?)$', t)):
		hh = int(m.group(2)) if m.group(1) else 0
		mm = int(m.group(3))
		ss = float(m.group(4))
		return hh, mm, ss

	return None

def main() -> int:
	args = {
		'LEVEL': None, 
		'TIME': None, 
		'ENTRY': None, 
	}

	if len(sys.argv) > 1:
		args = docopt(__doc__)
	else:
		while not args['LEVEL']:
			args['LEVEL'] = input('Enter level number (e.g. 31, 99): ')

		while not args['TIME']:
			args['TIME'] = input('Enter time (e.g. 13:37, 2:35:12.431): ')

		args['ENTRY'] = input('Enter entry time (or leave empty for 00:00): ')

	if not args['LEVEL'].isnumeric() or not LEVEL_OFFSET <= int(args['LEVEL']) <= 99:
		print('Invalid level provided.')
		return 1

	level = int(args['LEVEL']) - LEVEL_OFFSET
	time = parse_time(args['TIME'])

	if not time:
		print('Invalid time provided.')
		return 1

	if args['ENTRY']:
		entry = parse_time(args['ENTRY'])

		if not entry:
			print('Invalid entry time provided.')
			return 1
	else:
		entry = (0, 0, 0)

	dt = to_seconds(time) - to_seconds(entry)
	avg = dt / level

	print('Average level time:'.ljust(24), f'{avg:.2f}s')

	if level != 99 - LEVEL_OFFSET:
		proj = to_seconds(time) + (99 - LEVEL_OFFSET - level) * avg
		print('Projected finish time:'.ljust(24), datetime.fromtimestamp(proj, tz=timezone.utc).strftime('%H:%M:%S.%f')[:-3])

	if len(sys.argv) == 1:
		input('Press enter...')

try:
	exitcode = main()
	sys.exit(exitcode)
except KeyboardInterrupt:
	print('\nInterrupted.')
	sys.exit(130)